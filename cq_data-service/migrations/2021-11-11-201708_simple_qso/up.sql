create table if not exists "simple_qso" (
  "id" serial primary key,
  "time" TIMESTAMP not null default NOW(),
  "call" varchar(20) not null,
  "opcall" varchar(20) not null,
  "name" varchar(100) null,
  "qth" varchar(100) null,
  "mode" varchar(40) null,
  "frequency" FLOAT8 null,
  "tx_rapport" SMALLINT null,
  "rx_rapport" SMALLINT null
);

create table if not exists "advanced_qso" (
  "id" serial primary key
);

comment on column "simple_qso"."time" is 'time in UTC you did the QSO';
comment on column "simple_qso"."call" is 'Call sign of the person you spoke to ';
comment on column "simple_qso"."date" is 'the date you did the QSO ';
comment on column "simple_qso"."name" is 'Name of the operator you spoke to';
comment on column "simple_qso"."qth" is 'The QTH of the other person';
comment on column "simple_qso"."mode" is 'Mode you worked on ';
comment on column "simple_qso"."frequency" is 'Freqnecy you worked on';
comment on column "simple_qso"."tx_rapport" is 'Rapport TX'