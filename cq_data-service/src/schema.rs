table! {
    advanced_qso (id) {
        id -> Int4,
    }
}

table! {
    simple_qso (id) {
        id -> Int4,
        time -> Timestamp,
        call -> Varchar,
        opcall -> Varchar,
        name -> Nullable<Varchar>,
        qth -> Nullable<Varchar>,
        mode -> Nullable<Varchar>,
        frequency -> Nullable<Float8>,
        tx_rapport -> Nullable<Int2>,
        rx_rapport -> Nullable<Int2>,
    }
}

allow_tables_to_appear_in_same_query!(
    advanced_qso,
    simple_qso,
);
